window.addEventListener("load", loadedCompletedHTML);
	function loadedCompletedHTML()
	{
		//Menu Button SMP animation
		(function() {
			var menuBtn = $('a.menu-btn'),
				menuBtnBlock = menuBtn.find('div.menu-btn-block');
			menuBtn.on('click', function() {
				menuBtnBlock.toggleClass('active');
			});
		})();
	}

//Fixed Header
if(window.innerWidth < 736){
	var Nscroll = 93;
}else {
	var Nscroll = 260;
}
$(window).bind('scroll', function () {
	if ($(window).scrollTop() > Nscroll) {
		$('#header').addClass('Hfixed');
		$('.fixedHeaderBox').css("display", "block");
	} else {
		$('#header').removeClass('Hfixed');
		$('.fixedHeaderBox').css("display", "none");
	}
});

//Open SMP Menu (Mobile)
$(document).ready(function(){
$(".menu-btn").click(function(){
	$(this).toggleClass("on");
	$("#open_menu").slideToggle("slow");
});
});

//Open Sub SMP Menu (Mobile)
$(".click_menu_sub_smp").slideToggle("slow");
$(document).ready(function(){
	$('#open_menu > ul > li > a').on('click',function(){
			var checkElement = $(this).next();
			if((checkElement.is('ul')) && (checkElement.is(':visible'))) {
				checkElement.slideUp('normal');
			}
			if((checkElement.is('ul')) && (!checkElement.is(':visible'))) {
				$('#open_menu ul ul:visible').slideUp('normal');
				checkElement.slideDown('normal');
			}
			if($(this).closest('li').find('ul').children().length == 0) {
				return true;
			} else {
				return false;
			}
		});
	});

//Click Sub Menu SMP
$(document).ready(function(){
    $("ul li.sub1 a").click(function(){
        $(".Xbtn1").toggle();
		$(".Xbtn2").hide();
    });
});

$(document).ready(function(){
    $("ul li.sub2 a").click(function(){
        $(".Xbtn2").toggle();
		$(".Xbtn1").hide();
    });
});

$(document).ready(function(){
    $("li.sub1 ul.menu_sub_smp li a").click(function(){
		$(".Xbtn1").show();
    });
});
$(document).ready(function(){
    $("li.sub2 ul.menu_sub_smp li a").click(function(){
		$(".Xbtn2").show();
    });
});

//Gallery Active Effect Menu buttons
$(window).bind('scroll', function () {
	if ($(window).scrollTop() < 1000) {
		$('.GalleryActiveEffect').attr("src","images/Gallery_pc-active.png");
		$('.GalleryActiveEffect').removeClass('hoverEffect');
	} else {
		$('.GalleryActiveEffect').attr("src","images/Gallery_pc.png");
		$('.GalleryActiveEffect').addClass('hoverEffect');
	}
});
$(window).bind('scroll', function () {
	if ($(window).scrollTop() > 1000) {
		$('.AccessActiveEffect').attr("src","images/Access_pc-active.png");
		$('.AccessActiveEffect').removeClass('hoverEffect');
	} else {
		$('.AccessActiveEffect').attr("src","images/Access_pc.png");
		$('.AccessActiveEffect').addClass('hoverEffect');
	}
});

//Index Slider
$(window).on('load resize', function () {
  if (window.innerWidth < 736) {
    $('img').each(function() {
      $(this).attr('src', $(this).attr('data-smp'));
    });
  }
  else {
    $('img').each(function() {
      $(this).attr('src', $(this).attr('data-pc'));
    });
  }
});