//================================
//      Scroll to Top
//================================
$(document).ready(function(){
	//Check to see if the window is top if not then display button
    $(window).scroll(function(){
        if ($(this).scrollTop() > 115) {
            $('.btn-totop').fadeIn(200);
        } else {
            $('.btn-totop').fadeOut(200);
        }
    });
    //Click event to scroll to top
    $('.btn-totop').click(function(){
        $('html, body').animate({scrollTop : 0},400);
        return false;
    });
});

//================================
//      Button Effect
//================================
$(document).ready(function(){
    $(".btn").hover(
        function() {
            $(this).stop().animate({"opacity": "0"}, "fast");
    },
        function() {
            $(this).stop().animate({"opacity": "1"}, "fast");
        });
});

//Index opaque
$(function() {
    $('.eff-opaque_slow, .eff-opaque_normal, .eff-opaque_fast').on('inview', function(event, isInView, visiblePartX, visiblePartY) {
        if(isInView){
            $(this).stop().addClass('opa');
        }
        else{
            $(this).stop().removeClass('opa');
        }
    });
});


//================================
//      Case Toggle contents
//================================

    //Hide Other Cases on load
$(document).ready(function () {
    $(".case_2").hide();
    $(".case_3").hide();
    $("._toggle1").hide();
    $(".c-contact_content_3 div ._return").hide();
    $(".c-contact_content_3 div ._send").hide();
});

    //Navi
$(document).ready(function(){
    $("._toggle1").click(function(){
        $(".case_1").show();
        $(".case_2").hide();
        $(".case_3").hide();
        $("._toggle1").hide();
        $("._toggle2").show();
        $("._toggle3").show();
    });
});
$(document).ready(function(){
    $("._toggle2").click(function(){
        $(".case_1").hide();
        $(".case_2").show();
        $(".case_3").hide();
        $("._toggle1").show();
        $("._toggle2").hide();
        $("._toggle3").show();
    });
});
$(document).ready(function(){
    $("._toggle3").click(function(){
        $(".case_1").hide();
        $(".case_2").hide();
        $(".case_3").show();
        $("._toggle1").show();
        $("._toggle2").show();
        $("._toggle3").hide();
    });
});

//================================
//      Contact Submit Buttons
//================================
$(document).ready(function(){
    $(".c-contact_content_3 ._confirm").click(function(){
        $(".c-contact_content_3 div ._return").show();
        $(".c-contact_content_3 div ._send").show();
    });
});
$(document).ready(function(){
    $(".c-contact_content_3 div ._return").click(function(){
        $(".c-contact_content_3 div ._return").hide();
        $(".c-contact_content_3 div ._send").hide();
    });
});