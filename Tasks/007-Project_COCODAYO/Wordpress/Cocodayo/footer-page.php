<!--
================================================================
		F O O T E R

		@footer
================================================================
-->
<footer class="l-footer">
	<div class="l-container"><!-- ▼ Container ▼ -->
<!--
________________________________________________________________
-->
		<div class="c-footer_1">
			<!--
			________________________________
					Title
			-->
			<a class="c-footer_1__title">
				<!-- 💻 PC -->
				<img class="pc_only" src="<?php echo get_template_directory_uri(); ?>/assets/img/pc/title_footer.png" width="242" height="20" alt>
				<!-- 📱 SP -->
				<img class="sp_only" src="<?php echo get_template_directory_uri(); ?>/assets/img/sp/title_footer.png" width="1084" height="90" alt>
			</a>
			<!--
			________________________________
					Buttons
			-->
			<div class="c-footer_1__btn">
				<a href="https://itunes.apple.com/jp/app/id1156813708" target="_blank">
					<!-- 💻 PC -->
					<img class="pc_only u-hover_eff" src="<?php echo get_template_directory_uri(); ?>/assets/img/pc/btn-app_store.png" width="133" height="40" alt>
					<!-- 📱 SP -->
					<img class="sp_only" src="<?php echo get_template_directory_uri(); ?>/assets/img/sp/btn-app_store.png" width="648" height="196" alt>
				</a>

				<a href="https://play.google.com/store/apps/details?id=jp.cocodayo.consumer.android" target="_blank">
					<!-- 💻 PC -->
					<img class="pc_only u-hover_eff" src="<?php echo get_template_directory_uri(); ?>/assets/img/pc/btn-google_play.png" width="133" height="40" alt>
					<!-- 📱 SP -->
					<img class="sp_only" src="<?php echo get_template_directory_uri(); ?>/assets/img/sp/btn-google_play.png" width="648" height="196" alt>
				</a>
			</div>
			<!--
			________________________________
			-->
		</div>
<!--
________________________________________________________________
-->
		<div class="c-footer_2">
			<!--
			________________________________
					Logo
			-->
			<a class="c-footer_2__logo e-goto_url" href="<?php echo site_url(); ?>">
				<!-- 💻 PC -->
				<img class="pc_only" src="<?php echo get_template_directory_uri(); ?>/assets/img/pc/logo_footer.png" width="174" height="50" alt>
				<!-- 📱 SP -->
				<img class="sp_only" src="<?php echo get_template_directory_uri(); ?>/assets/img/sp/logo_white.png" width="678" height="194" alt>
			</a>
			<!--
			________________________________
					Navi
			-->
			<?php wp_nav_menu( array(
				'theme_location' => 'footer-menu',
				'container_class' => 'c-footer_2__navi' ) );
			?>
			<!--
			________________________________
					💻 PC Social
			-->
			<div class="c-footer_2__social pc_only">
				<a class="u-hover_eff" href="https://twitter.com/cocodayo_anshin">
					<img src="<?php echo get_template_directory_uri(); ?>/assets/img/pc/icon-twitter.png" width="44" height="44" alt>
				</a>

				<a class="u-hover_eff" href="https://www.facebook.com/cocodayo.genetec/">
					<img src="<?php echo get_template_directory_uri(); ?>/assets/img/pc/icon-facebook.png" width="44" height="44" alt>
				</a>
			</div>
			<!--
			________________________________
			-->
		</div>
<!--
________________________________________________________________
		Copyright
-->
		<div class="c-footer_3">
			<p>※製品に関するお問合せは、App Store/Google Play、<br class="sp_only">またはアプリケーション内のメニューからお願いします<br>
			Copyright© 2016 GENETEC Corp. <br class="sp_only">All Rights Reserved.</p>
		</div>
<!--
________________________________________________________________
-->
	</div><!-- ▲ Container ▲ -->
</footer>

</body>
</html>