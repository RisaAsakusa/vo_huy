<!DOCTYPE html>
<html lang="ja">
<head>
<!--meta-->
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="COCODAYO">
<meta name="keywords" content="COCODAYO">
<!--title-->
<title>COCODAYO - <?php single_post_title(); ?></title>
<!--favicon-->
<link rel="icon" href="<?php echo get_template_directory_uri(); ?>/assets/img/favicon.png" type="image/png">
<!--css-->
<link href="<?php echo get_template_directory_uri(); ?>/assets/css/style.css" rel="stylesheet">
<!--js-->
<script src="<?php echo get_template_directory_uri(); ?>/assets/js/common.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/assets/js/jquery.easeScroll.js"></script>
<script>
//================================
//		Fixed Header effect
//================================
if(window.innerWidth < 768){
	//---- 💻 PC ----
	$(window).scroll(function(){
		if ($(this).scrollTop() > 1) {
			$(".c-header").addClass("is-fixed");
			//$(".l-header__index .c-btn_MenuSP__bar1, .l-header__index .c-btn_MenuSP__bar2, .l-header__index .c-btn_MenuSP__bar3").css('background-color', 'white');
			$(".l-header__index .c-header__logo a .js-logo").attr('src','<?php echo get_template_directory_uri(); ?>/assets/img/sp/logo_white.png');
			$(".l-header__index .c-btn_social_header .js-facbook img").attr('src','<?php echo get_template_directory_uri(); ?>/assets/img/sp/icon-facebook_white.png');
			$(".l-header__index .c-btn_social_header .js-twitter img").attr('src','<?php echo get_template_directory_uri(); ?>/assets/img/sp/icon-twitter_white.png');
			$(".l-header__index .c-btn_MenuSP").addClass("is-fixed_btnmenu");
		} else {
			$(".c-header").removeClass("is-fixed");
			//$(".l-header__index .c-btn_MenuSP__bar1, .l-header__index .c-btn_MenuSP__bar2, .l-header__index .c-btn_MenuSP__bar3").css('background-color', 'black');
			$(".l-header__index .c-header__logo a .js-logo").attr('src','<?php echo get_template_directory_uri(); ?>/assets/img/sp/logo.png');
			$(".l-header__index .c-btn_social_header .js-facbook img").attr('src','<?php echo get_template_directory_uri(); ?>/assets/img/sp/icon-facebook.png');
			$(".l-header__index .c-btn_social_header .js-twitter img").attr('src','<?php echo get_template_directory_uri(); ?>/assets/img/sp/icon-twitter.png');
			$(".l-header__index .c-btn_MenuSP").removeClass("is-fixed_btnmenu");
		}
	});

}else {

	//---- 📱 SP ----
	$(window).scroll(function(){
		if ($(this).scrollTop() > 1) {
			$(".c-header").addClass("is-fixed");
		} else {
			$(".c-header").removeClass("is-fixed");
		}
	});
}
</script>
</head>
<body class="preload">
<!--
================================================================
		H E A D E R

		@header
================================================================
-->
<header class="l-header">
<!--
________________________________________________________________
-->
	<div class="c-header"><!-- ? c-header ? -->
		<!--
		________________________________
				Logo
		-->
		<div class="c-header__logo">
			<a class="u-hover_eff e-goto_url" href="<?php echo site_url(); ?>">
				<!-- ?? PC -->
				<img class="pc_only" src="<?php echo get_template_directory_uri(); ?>/assets/img/pc/logo.png" width="182" height="52" alt>
				<!-- ?? SP -->
				<img class="sp_only" src="<?php echo get_template_directory_uri(); ?>/assets/img/sp/logo_1.png" width="426" height="122" alt>
			</a>
		</div>
		<!--
		________________________________
				?? SP Social Buttons
		-->
		<div class="c-btn_social_header sp_only">
			<a href="https://www.facebook.com/cocodayo.genetec/" target="_blank">
				<img src="<?php echo get_template_directory_uri(); ?>/assets/img/sp/icon-facebook.png" width="106" height="105" alt>
			</a>
			<a href="https://twitter.com/cocodayo_anshin" target="_blank">
				<img src="<?php echo get_template_directory_uri(); ?>/assets/img/sp/icon-twitter.png" width="110" height="105" alt>
			</a>
		</div>
		<!--
		________________________________
		-->
	</div><!-- ? c-header ? -->
<!--
________________________________________________________________
			?? SP Menu Button
-->
	<div class="c-btn_MenuSP sp_only" onclick="open_SP_Menu(this)">
		<div class="c-btn_MenuSP__bar1"></div>
		<div class="c-btn_MenuSP__bar2"></div>
		<div class="c-btn_MenuSP__bar3"></div>
	</div>
	<!--
	________________________________
	-->
	<nav class="c-gnavi"><!-- ? gnavi ? -->
		<?php wp_nav_menu( array( 'theme_location' => 'header-menu' ) ); ?>
		<ul>
			<!--
			________________________________
					Download
			-->
			<!-- 4 -->
			<li>
				<!-- ?? PC -->
				<p class="pc_only">今すぐ無料でダウンロード</p>
				<!-- ?? SP -->
				<div><img class="sp_only" src="<?php echo get_template_directory_uri(); ?>/assets/img/sp/download.png" width="754" height="62" alt></div>
			</li>
			<!--
			________________________________
					Buttons
			-->
			<!-- 5 -->
			<li><a class="u-hover_eff" href="https://itunes.apple.com/jp/app/id1156813708" target="_blank">
				<!-- ?? PC -->
				<img class="pc_only" src="<?php echo get_template_directory_uri(); ?>/assets/img/pc/btn-app_store.png" width="122" height="38" alt>
				<!-- ?? SP -->
				<img class="sp_only" src="<?php echo get_template_directory_uri(); ?>/assets/img/sp/btn-app_store.png" width="648" height="196" alt>
			</a></li>

			<!-- 6 -->
			<li><a class="u-hover_eff" href="https://play.google.com/store/apps/details?id=jp.cocodayo.consumer.android" target="_blank">
				<!-- ?? PC -->
				<img class="pc_only" src="<?php echo get_template_directory_uri(); ?>/assets/img/pc/btn-google_play.png" width="122" height="38" alt>
				<!-- ?? SP -->
				<img class="sp_only" src="<?php echo get_template_directory_uri(); ?>/assets/img/sp/btn-google_play.png" width="648" height="196" alt>
			</a></li>
			<!--
			________________________________
			-->
		</ul>
	</nav><!-- ? gnavi ? -->
<!--
________________________________________________________________
-->
</header>