<?php get_header(); ?>

<!--
================================================================
		M A I N

		@main
================================================================
-->
<main class="l-main p-index">
<!--
________________________________________________________________
			Index Navigation
-->
	<nav class="c-index_navi pc_only">
		<ul>
			<li class="is-current"><a>●</a></li>
			<li><a>●</a></li>
			<li><a>●</a></li>
			<li><a>●</a></li>
			<li><a>●</a></li>
			<li><a>●</a></li>
		</ul>
	</nav>
	<!--
	________________________________
			Banner
	-->
	<div class="c-banner_index">
		<!-- 💻 PC -->
		<img class="pc_only" src="<?php echo get_template_directory_uri(); ?>/assets/img/pc/logo_big.png" width="232" height="244" alt>
		<!-- 📱 SP -->
		<img class="sp_only" src="<?php echo get_template_directory_uri(); ?>/assets/img/sp/logo_big.png" width="962" height="1010" alt>
	</div>
<!--
________________________________________________________________
-->
	<!--
	================================
			Section 1    @sec1
	================================
	-->
	<section class="p-index_1">
		<div class="l-container"><!-- ▼ Container ▼ -->
			<!--
			________________________________
					Big Cell phone
			-->
			<div class="c-phone_top pc_only">
				<img src="<?php echo get_template_directory_uri(); ?>/assets/img/pc/phone_top.png" width="244" height="471" alt>
			</div>
			<!--
			________________________________
					Inner Section
			-->
			<div class="p-index_1__inner"><!-- ▼ Inner ▼ -->
				<!--
				________________________________
						Title
				-->
				<h2>
					<span class="pc_only">&nbsp;</span>
					<!-- 💻 PC -->
					<img class="pc_only" src="<?php echo get_template_directory_uri(); ?>/assets/img/pc/title_top.png" width="502" height="78" alt>
					<!-- 📱 SP -->
					<img class="sp_only" src="<?php echo get_template_directory_uri(); ?>/assets/img/sp/title_top.png" width="1388" height="206" alt>
					<span class="pc_only">&nbsp;</span>
				</h2>
				<!--
				________________________________
						Text Image
				-->
				<div class="l-txt_img"><!-- ▼ Text Image ▼ -->
					<!--
					________________________________
							1
					-->
					<div class="c-txt_img">
						<div class="c-txt_img__img">
							<!-- 💻 PC -->
							<img class="pc_only" src="<?php echo get_template_directory_uri(); ?>/assets/img/pc/icon-top_1.png" width="124" height="124" alt="">
							<!-- 📱 SP -->
							<img class="sp_only" src="<?php echo get_template_directory_uri(); ?>/assets/img/sp/icon-top_1.png" width="384" height="384" alt="">
						</div>

						<div class="c-txt_img__txt">
							<p>震度5以上の<br>地震を即通知</p>
						</div>
					</div>
					<!--
					________________________________
							2
					-->
					<div class="c-txt_img">
						<div class="c-txt_img__img">
							<!-- 💻 PC -->
							<img class="pc_only" src="<?php echo get_template_directory_uri(); ?>/assets/img/pc/icon-top_2.png" width="124" height="124" alt="">
							<!-- 📱 SP -->
							<img class="sp_only" src="<?php echo get_template_directory_uri(); ?>/assets/img/sp/icon-top_2.png" width="384" height="384" alt="">
						</div>

						<div class="c-txt_img__txt">
							<p>家族間チャットで<br>会話可能</p>
						</div>
					</div>
					<!--
					________________________________
							3
					-->
					<div class="c-txt_img">
						<div class="c-txt_img__img">
							<!-- 💻 PC -->
							<img class="pc_only" src="<?php echo get_template_directory_uri(); ?>/assets/img/pc/icon-top_3.png" width="124" height="124" alt="">
							<!-- 📱 SP -->
							<img class="sp_only" src="<?php echo get_template_directory_uri(); ?>/assets/img/sp/icon-top_3.png" width="384" height="384" alt="">
						</div>

						<div class="c-txt_img__txt">
							<p>家族の安否＆<br>居場所がわかる</p>
						</div>
					</div>
					<!--
					________________________________
					-->
				</div><!-- ▲ Text Image ▲ -->
			</div><!-- ▲ Inner ▲ -->
		</div><!-- ▲ Container ▲ -->
	</section>
<!--
________________________________________________________________
-->
	<!--
	================================
			Section 2    @sec2
	================================
	-->
	<section class="p-index_2">
		<div class="l-container"><!-- ▼ Container ▼ -->
			<!--
			________________________________
					Title
			-->
			<h2 class="c-title_index">
				<span class="pc_only">&nbsp;</span>
				<!-- 💻 PC -->
				<img class="pc_only" src="<?php echo get_template_directory_uri(); ?>/assets/img/pc/title_about.png" width="366" height="98" alt>
				<!-- 📱 SP -->
				<img class="sp_only" src="<?php echo get_template_directory_uri(); ?>/assets/img/sp/title_about.png" width="1280" height="338" alt>
				<span class="pc_only">&nbsp;</span>
			</h2>
			<!--
			________________________________
					Text
			-->
			<p>通学・通勤中、習いごと、出張。いつわたしたちの生活を揺るがすかわからない大地震。家族が一緒にいるときに起こるとは限りません。有事への備えは自主防災の意識から。大きな不安を少しでも和らげるのは、家族の安否と「ここだよ」「無事だよ」の言葉。COCODAYOは、大切な家族をつなぐお守りアプリです。</p>
		</div><!-- ▲ Container ▲ -->
		<!--
		________________________________
				Video
		-->
		<div class="c-video">
			<iframe src="https://www.youtube.com/embed/JYxTnaHbTag?rel=0" allowfullscreen></iframe>
			<!--
			________________________________
					Button
			-->
			<a>
				<!-- 💻 PC -->
				<img class="pc_only" src="<?php echo get_template_directory_uri(); ?>/assets/img/pc/btn-video.png" width="320" height="68" alt>
				<!-- 📱 SP -->
				<img class="sp_only" src="<?php echo get_template_directory_uri(); ?>/assets/img/sp/btn-video.png" width="790" height="164" alt>
			</a>
		</div>
		<!--
		________________________________
		-->
	</section>
<!--
________________________________________________________________
-->
	<!--
	================================
			Section 3    @sec3
	================================
	-->
	<section class="p-index_3">
		<div class="l-container"><!-- ▼ Container ▼ -->
			<!--
			________________________________
					Title
			-->
			<h2 class="c-title_index_1">
				<span class="pc_only">&nbsp;</span>
				<!-- 💻 PC -->
				<img class="pc_only" src="<?php echo get_template_directory_uri(); ?>/assets/img/pc/subtitle-topics.png" width="180" height="44" alt>
				<!-- 📱 SP -->
				<img class="sp_only" src="<?php echo get_template_directory_uri(); ?>/assets/img/sp/subtitle-topics.png" width="618" height="154" alt>
				<span class="pc_only">&nbsp;</span>
			</h2>
			<!--
			________________________________
					Text
			-->
			<p>『ココダヨ』では、毎月1日 午前10時 に「訓練地震」を実施いたします。<br class="pc_only">
			訓練地震では、本当の地震が発生した場合と同様に、ココダヨユーザ全端末に 最大震度予測5弱の地震が発生したという「訓練の」通知を発信いたします。次回実施予定は 2017年6月1日 木曜日 午前10時 です。</p>
			<!--
			________________________________
					Posts
			-->
			<!--<div class="c-txt_line">
				<a class="e-goto_url" href="index.html">
					<p><span>2017.3.14</span>
					『ココダヨ』アプリiOS版を、1.1.9にバージョンアップしました。</p>
				</a>
				<a class="e-goto_url" href="index.html">
					<p><span>2017.3.14</span>
					『ココダヨ』アプリiOS版を、1.1.9にバージョンアップしました。</p>
				</a>
				<a class="e-goto_url" href="index.html">
					<p><span>2017.3.14</span>
					『ココダヨ』アプリiOS版を、1.1.9にバージョンアップしました。</p>
				</a>
			</div>-->
			<div class="c-txt_line">
			<?php if ( have_posts() ) : ?>
				<?php query_posts('showposts=3'); ?>
				<?php while ( have_posts() ) : the_post(); ?>
					<a class="e-goto_url" href="/topics">
						<p>
							<span><?php echo get_the_date(); ?></span>
							<?php echo get_the_title(); ?>
						</p>
					</a>
				<?php endwhile; ?>
			<?php endif; ?>
			</div>
			<!--
			________________________________
					Link Button
			-->
			<a class="c-btn_link u-fr e-goto_url" href="/topics">トピックス一覧へ</a>
			<!--
			________________________________
			-->
		</div><!-- ▲ Container ▲ -->
	</section>
<!--
________________________________________________________________
-->
	<!--
	================================
			Section 4    @sec4
	================================
	-->
	<section class="p-index_4">
		<div class="l-container"><!-- ▼ Container ▼ -->
			<!--
			________________________________
					Title
			-->
			<h2 class="c-title_index">
				<span class="pc_only">&nbsp;</span>
				<!-- 💻 PC -->
				<img class="pc_only" src="<?php echo get_template_directory_uri(); ?>/assets/img/pc/title_how.png" width="736" height="80" alt>
				<!-- 📱 SP -->
				<img class="sp_only" src="<?php echo get_template_directory_uri(); ?>/assets/img/sp/title_how.png" width="1280" height="490" alt>
				<span class="pc_only">&nbsp;</span>
			</h2>
			<!--
			________________________________
					Sliders
			-->
			<div class="l-slider_index">
				<!--
				________________________________
						Slider 1
				-->
				<div class="swiper-container c-slider_index"><!-- ▼ Slider container ▼ -->
					<div class="swiper-wrapper c-slider_index__wrapper"><!-- ▼ Wrapper ▼ -->
						<!--
						________________________________
								1
						-->
						<div class="swiper-slide c-slider_index__slide"><!-- ▼ Slider ▼ -->
							<!--
							________________________________
							-->
							<div class="c-slider_index__col_1">
								<!-- 💻 PC -->
								<img class="pc_only" src="<?php echo get_template_directory_uri(); ?>/assets/img/pc/phone_point_1.png" width="240" height="428" alt>
								<!-- 📱 SP -->
								<img class="sp_only" src="<?php echo get_template_directory_uri(); ?>/assets/img/sp/phone_point_1.png" width="926" height="1644" alt>
							</div>
							<!--
							________________________________
							-->
							<div class="c-slider_index__col_2">
								<!-- 💻 PC -->
								<img class="pc_only" src="<?php echo get_template_directory_uri(); ?>/assets/img/pc/point_1.png" width="314" height="78" alt>
								<!-- 📱 SP -->
								<img class="sp_only" src="<?php echo get_template_directory_uri(); ?>/assets/img/sp/point_1.png" width="850" height="230" alt>

								<p>大きな地震警報発令時は、アプリ上辺の背景が赤になります。</p>
							</div>
							<!--
							________________________________
							-->
						</div><!-- ▲ Slider ▲ -->
						<!--
						________________________________
								2
						-->
						<div class="swiper-slide c-slider_index__slide"><!-- ▼ Slider ▼ -->
							<!--
							________________________________
							-->
							<div class="c-slider_index__col_1">
								<!-- 💻 PC -->
								<img class="pc_only" src="<?php echo get_template_directory_uri(); ?>/assets/img/pc/phone_point_1_1.png" width="240" height="428" alt>
								<!-- 📱 SP -->
								<img class="sp_only" src="<?php echo get_template_directory_uri(); ?>/assets/img/sp/phone_point_1_1.png" width="926" height="1644" alt>
							</div>
							<!--
							________________________________
							-->
							<div class="c-slider_index__col_2">
								<!-- 💻 PC -->
								<img class="pc_only" src="<?php echo get_template_directory_uri(); ?>/assets/img/pc/point_1.png" width="314" height="78" alt>
								<!-- 📱 SP -->
								<img class="sp_only" src="<?php echo get_template_directory_uri(); ?>/assets/img/sp/point_1.png" width="850" height="230" alt>

								<p>左に震央震度、右に発生した日時、震央の場所、マグニチュードが表示されます。</p>
							</div>
							<!--
							________________________________
							-->
						</div><!-- ▲ Slider ▲ -->
						<!--
						________________________________
								3
						-->
						<div class="swiper-slide c-slider_index__slide"><!-- ▼ Slider ▼ -->
							<!--
							________________________________
							-->
							<div class="c-slider_index__col_1">
								<!-- 💻 PC -->
								<img class="pc_only" src="<?php echo get_template_directory_uri(); ?>/assets/img/pc/phone_point_1_2.png" width="240" height="428" alt>
								<!-- 📱 SP -->
								<img class="sp_only" src="<?php echo get_template_directory_uri(); ?>/assets/img/sp/phone_point_1_2.png" width="926" height="1644" alt>
							</div>
							<!--
							________________________________
							-->
							<div class="c-slider_index__col_2">
								<!-- 💻 PC -->
								<img class="pc_only" src="<?php echo get_template_directory_uri(); ?>/assets/img/pc/point_1.png" width="314" height="78" alt>
								<!-- 📱 SP -->
								<img class="sp_only" src="<?php echo get_template_directory_uri(); ?>/assets/img/sp/point_1.png" width="850" height="230" alt>

								<p>無事を知らせるように求めるボタンが表示されるので、押せる状態であれば無事を知らせてください。</p>
							</div>
							<!--
							________________________________
							-->
						</div><!-- ▲ Slider ▲ -->
						<!--
						________________________________
						-->
					</div><!-- ▲ Wrapper ▲ -->
					<!--
					________________________________
							Slider buttons
					-->
					<!-- Pagination -->
					<div class="swiper-pagination"></div>
					<!-- Add Arrows -->
					<div class="swiper-button-next"></div>
					<div class="swiper-button-prev"></div>
				</div><!-- ▲ Slider container ▲ -->
				<!--
				________________________________
						Slider 2
				-->
				<div class="swiper-container c-slider_index"><!-- ▼ Slider container ▼ -->
					<div class="swiper-wrapper c-slider_index__wrapper"><!-- ▼ Wrapper ▼ -->
						<!--
						________________________________
								1
						-->
						<div class="swiper-slide c-slider_index__slide"><!-- ▼ Slider ▼ -->
							<!--
							________________________________
							-->
							<div class="c-slider_index__col_1">
								<!-- 💻 PC -->
								<img class="pc_only" src="<?php echo get_template_directory_uri(); ?>/assets/img/pc/phone_point_2.png" width="240" height="428" alt>
								<!-- 📱 SP -->
								<img class="sp_only" src="<?php echo get_template_directory_uri(); ?>/assets/img/sp/phone_point_2.png" width="926" height="1644" alt>
							</div>
							<!--
							________________________________
							-->
							<div class="c-slider_index__col_2">
								<!-- 💻 PC -->
								<img class="pc_only" src="<?php echo get_template_directory_uri(); ?>/assets/img/pc/point_2.png" width="372" height="78" alt>
								<!-- 📱 SP -->
								<img class="sp_only" src="<?php echo get_template_directory_uri(); ?>/assets/img/sp/point_2.png" width="850" height="230" alt>

								<p>通常時、位置は非表示になっています。「都道府県」「市区町村」「町区、番地」レベルに変更可能です。</p>
							</div>
							<!--
							________________________________
							-->
						</div><!-- ▲ Slider ▲ -->
						<!--
						________________________________
								2
						-->
						<div class="swiper-slide c-slider_index__slide"><!-- ▼ Slider ▼ -->
							<!--
							________________________________
							-->
							<div class="c-slider_index__col_1">
								<!-- 💻 PC -->
								<img class="pc_only" src="<?php echo get_template_directory_uri(); ?>/assets/img/pc/phone_point_2_1.png" width="240" height="428" alt>
								<!-- 📱 SP -->
								<img class="sp_only" src="<?php echo get_template_directory_uri(); ?>/assets/img/sp/phone_point_2_1.png" width="926" height="1644" alt>
							</div>
							<!--
							________________________________
							-->
							<div class="c-slider_index__col_2">
								<!-- 💻 PC -->
								<img class="pc_only" src="<?php echo get_template_directory_uri(); ?>/assets/img/pc/point_2.png" width="372" height="78" alt>
								<!-- 📱 SP -->
								<img class="sp_only" src="<?php echo get_template_directory_uri(); ?>/assets/img/sp/point_2.png" width="850" height="230" alt>

								<p>住所が詳しい表示になり、家族の場所が特定しやすくなります。住所をタップすると、地図アプリが開き、位置が表示されます。</p>
							</div>
							<!--
							________________________________
							-->
						</div><!-- ▲ Slider ▲ -->
						<!--
						________________________________
								3
						-->
						<div class="swiper-slide c-slider_index__slide"><!-- ▼ Slider ▼ -->
							<!--
							________________________________
							-->
							<div class="c-slider_index__col_1">
								<!-- 💻 PC -->
								<img class="pc_only" src="<?php echo get_template_directory_uri(); ?>/assets/img/pc/phone_point_2_2.png" width="240" height="428" alt>
								<!-- 📱 SP -->
								<img class="sp_only" src="<?php echo get_template_directory_uri(); ?>/assets/img/sp/phone_point_2_2.png" width="926" height="1644" alt>
							</div>
							<!--
							________________________________
							-->
							<div class="c-slider_index__col_2">
								<!-- 💻 PC -->
								<img class="pc_only" src="<?php echo get_template_directory_uri(); ?>/assets/img/pc/point_2.png" width="372" height="78" alt>
								<!-- 📱 SP -->
								<img class="sp_only" src="<?php echo get_template_directory_uri(); ?>/assets/img/sp/point_2.png" width="850" height="230" alt>

								<p>住所をタップすると、地図アプリが開き、位置が表示されます。</p>
							</div>
							<!--
							________________________________
							-->
						</div><!-- ▲ Slider ▲ -->
						<!--
						________________________________
						-->
					</div><!-- ▲ Wrapper ▲ -->
					<!--
					________________________________
							Slider buttons
					-->
					<!-- Pagination -->
					<div class="swiper-pagination"></div>
					<!-- Add Arrows -->
					<div class="swiper-button-next"></div>
					<div class="swiper-button-prev"></div>
				</div><!-- ▲ Slider container ▲ -->
				<!--
				________________________________
						Slider 3
				-->
				<div class="swiper-container c-slider_index"><!-- ▼ Slider container ▼ -->
					<div class="swiper-wrapper c-slider_index__wrapper"><!-- ▼ Wrapper ▼ -->
						<!--
						________________________________
								1
						-->
						<div class="swiper-slide c-slider_index__slide"><!-- ▼ Slider ▼ -->
							<!--
							________________________________
							-->
							<div class="c-slider_index__col_1">
								<!-- 💻 PC -->
								<img class="pc_only" src="<?php echo get_template_directory_uri(); ?>/assets/img/pc/phone_point_3.png" width="240" height="428" alt>
								<!-- 📱 SP -->
								<img class="sp_only" src="<?php echo get_template_directory_uri(); ?>/assets/img/sp/phone_point_3.png" width="926" height="1644" alt>
							</div>
							<!--
							________________________________
							-->
							<div class="c-slider_index__col_2">
								<!-- 💻 PC -->
								<img class="pc_only" src="<?php echo get_template_directory_uri(); ?>/assets/img/pc/point_3.png" width="372" height="78" alt>
								<!-- 📱 SP -->
								<img class="sp_only" src="<?php echo get_template_directory_uri(); ?>/assets/img/sp/point_3.png" width="850" height="230" alt>

								<p>大きな地震警報発令時は、アプリ上辺の背景が赤になります。</p>
							</div>
							<!--
							________________________________
							-->
						</div><!-- ▲ Slider ▲ -->
						<!--
						________________________________
								2
						-->
						<div class="swiper-slide c-slider_index__slide"><!-- ▼ Slider ▼ -->
							<!--
							________________________________
							-->
							<div class="c-slider_index__col_1">
								<!-- 💻 PC -->
								<img class="pc_only" src="<?php echo get_template_directory_uri(); ?>/assets/img/pc/phone_point_3_1.png" width="240" height="428" alt>
								<!-- 📱 SP -->
								<img class="sp_only" src="<?php echo get_template_directory_uri(); ?>/assets/img/sp/phone_point_3_1.png" width="926" height="1644" alt>
							</div>
							<!--
							________________________________
							-->
							<div class="c-slider_index__col_2">
								<!-- 💻 PC -->
								<img class="pc_only" src="<?php echo get_template_directory_uri(); ?>/assets/img/pc/point_3.png" width="372" height="78" alt>
								<!-- 📱 SP -->
								<img class="sp_only" src="<?php echo get_template_directory_uri(); ?>/assets/img/sp/point_3.png" width="850" height="230" alt>

								<p>自分の家族とチャットします。電話連絡よりも短時間で全員で状況を共有します。</p>
							</div>
							<!--
							________________________________
							-->
						</div><!-- ▲ Slider ▲ -->
						<!--
						________________________________
								3
						-->
						<div class="swiper-slide c-slider_index__slide"><!-- ▼ Slider ▼ -->
							<!--
							________________________________
							-->
							<div class="c-slider_index__col_1">
								<!-- 💻 PC -->
								<img class="pc_only" src="<?php echo get_template_directory_uri(); ?>/assets/img/pc/phone_point_3_2.png" width="240" height="428" alt>
								<!-- 📱 SP -->
								<img class="sp_only" src="<?php echo get_template_directory_uri(); ?>/assets/img/sp/phone_point_3_2.png" width="926" height="1644" alt>
							</div>
							<!--
							________________________________
							-->
							<div class="c-slider_index__col_2">
								<!-- 💻 PC -->
								<img class="pc_only" src="<?php echo get_template_directory_uri(); ?>/assets/img/pc/point_3.png" width="372" height="78" alt>
								<!-- 📱 SP -->
								<img class="sp_only" src="<?php echo get_template_directory_uri(); ?>/assets/img/sp/point_3.png" width="850" height="230" alt>

								<p>家族だけではなく、実家など他のグループの無事も同時に確認でき、チャットでもやり取りできます。</p>
							</div>
							<!--
							________________________________
							-->
						</div><!-- ▲ Slider ▲ -->
						<!--
						________________________________
								4
						-->
						<div class="swiper-slide c-slider_index__slide"><!-- ▼ Slider ▼ -->
							<!--
							________________________________
							-->
							<div class="c-slider_index__col_1">
								<!-- 💻 PC -->
								<img class="pc_only" src="<?php echo get_template_directory_uri(); ?>/assets/img/pc/phone_point_3_3.png" width="240" height="428" alt>
								<!-- 📱 SP -->
								<img class="sp_only" src="<?php echo get_template_directory_uri(); ?>/assets/img/sp/phone_point_3_3.png" width="926" height="1644" alt>
							</div>
							<!--
							________________________________
							-->
							<div class="c-slider_index__col_2">
								<!-- 💻 PC -->
								<img class="pc_only" src="<?php echo get_template_directory_uri(); ?>/assets/img/pc/point_3.png" width="372" height="78" alt>
								<!-- 📱 SP -->
								<img class="sp_only" src="<?php echo get_template_directory_uri(); ?>/assets/img/sp/point_3.png" width="850" height="230" alt>

								<p>緊急時こそ安全を確認しあう、家族など密接なグループ向け専用なので、日常でも安心してチャットできます。</p>
							</div>
							<!--
							________________________________
							-->
						</div><!-- ▲ Slider ▲ -->
						<!--
						________________________________
						-->
					</div><!-- ▲ Wrapper ▲ -->
					<!--
					________________________________
							Slider buttons
					-->
					<!-- Pagination -->
					<div class="swiper-pagination"></div>
					<!-- Add Arrows -->
					<div class="swiper-button-next"></div>
					<div class="swiper-button-prev"></div>
				</div><!-- ▲ Slider container ▲ -->
				<!--
				________________________________
				-->
			</div>
			<!--
			________________________________
					Slider's Script
			-->
			<!-- JS -->
			<script src="<?php echo get_template_directory_uri(); ?>/assets/js/swiper.min.js"></script>

			<!-- Initialize -->
			<script>
				var swiper = new Swiper('.swiper-container', {
					//"cube" or "fade"
					effect: 'fade',
					loop: true,
					pagination: {
						el: '.swiper-pagination',
						clickable: true,
					},
					navigation: {
						nextEl: '.swiper-button-next',
						prevEl: '.swiper-button-prev',
					},
					autoplay: {
						delay: 15000,
						disableOnInteraction: false,
					},
				});
			</script>
			<!--
			________________________________
			-->
		</div><!-- ▲ Container ▲ -->
	</section>
<!--
________________________________________________________________
-->
	<!--
	================================
			Section 5    @sec5
	================================
	-->
	<section class="p-index_5">
		<div class="l-container"><!-- ▼ Container ▼ -->
			<!--
			________________________________
					Title
			-->
			<h2 class="c-title_index_1">
				<span class="pc_only">&nbsp;</span>
				<!-- 💻 PC -->
				<img class="pc_only" src="<?php echo get_template_directory_uri(); ?>/assets/img/pc/subtitle-genetec.png" width="374" height="46" alt>
				<!-- 📱 SP -->
				<img class="sp_only" src="<?php echo get_template_directory_uri(); ?>/assets/img/sp/subtitle-genetec.png" width="1244" height="154" alt>
				<span class="pc_only">&nbsp;</span>
			</h2>
			<!--
			________________________________
					Images
			-->
			<div class="l-img">
				<!--
				________________________________
				-->
				<div class="c-img">
					<!-- 💻 PC -->
					<img class="pc_only" src="<?php echo get_template_directory_uri(); ?>/assets/img/pc/genetec_img_1.png" width="376" height="140" alt>
					<!-- 📱 SP -->
					<img class="sp_only" src="<?php echo get_template_directory_uri(); ?>/assets/img/sp/genetec_img_2.png" width="1266" height="472" alt>
				</div>
				<!--
				________________________________
				-->
				<div class="c-img">
					<!-- 💻 PC -->
					<img class="pc_only" src="<?php echo get_template_directory_uri(); ?>/assets/img/pc/genetec_img_2.png" width="376" height="140" alt>
					<!-- 📱 SP -->
					<img class="sp_only" src="<?php echo get_template_directory_uri(); ?>/assets/img/sp/genetec_img_1.png" width="1266" height="472" alt>
				</div>
				<!--
				________________________________
				-->
			</div>
		</div><!-- ▲ Container ▲ -->
	</section>
<!--
________________________________________________________________
-->
</main>

<?php get_footer(); ?>