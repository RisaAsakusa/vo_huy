<?php
/*
 * Template Name: TOPICS
 * Template Post Type: page
 */
get_header('page'); ?>

<!--
================================================================
		M A I N

		@main
================================================================
-->
<main class="l-main p-topics">
<!--
________________________________________________________________
		Banner
-->
	<div class="c-banner">
		<!-- 💻 PC -->
		<img class="pc_only" src="<?php echo get_template_directory_uri(); ?>/assets/img/pc/banner_title-topics.png" width="188" height="46" alt>
		<!-- 📱 SP -->
		<img class="sp_only" src="<?php echo get_template_directory_uri(); ?>/assets/img/pc/banner_title-topics.png" width="632" height="154" alt>
	</div>
<!--
________________________________________________________________
-->
	<!--
	================================
			Section 1    @sec1
	================================
	-->
	<section class="p-topics_1">
		<div class="l-container"><!-- ▼ Container ▼ -->
			<!--
			________________________________
					Text
			-->
			<h2>『ココダヨ』では、毎月1日 午前10時 に「訓練地震」を実施いたします。<br class="pc_only">
				訓練地震では、本当の地震が発生した場合と同様に、ココダヨユーザ全端末に最大震度予測5弱の地震が発生したという「訓練の」通知を発信いたします。次回実施予定は 2017年6月1日 木曜日 午前10時 です。
			</h2>
			<!--
			________________________________
					Text accordion
			-->
			<div class="l-txt_accordion"><!-- ▼ Text accordion ▼ -->
				<!--
				________________________________
						1

				<div class="c-txt_accordion">
					<h3><span>2017.3.14</span>『ココダヨ』アプリiOS版を、1.1.9にバージョンアップしました。</h3>

					<p class="c-txt_accordion__p1">たいへんお待たせ致しました。<br>
					リーダーになって自分のグループを作ることができるようになった<br>
					『ココダヨ』アプリAndroid版バージョン 1.1.0 をGoogle Play にて公開しました。<br>
					ご家族全員で Android をお使いの皆様、この機会に是非おためし下さい。<br>
					<a class="c-btn_link_1" href="#">Press Release →</a>
					<br><br>
					バージョン 1.1.0 の修正点<br>
					グループ作成機能：グループを作成してリーダーとなり、メンバーを招待できるようになりました。<br>
					データ引継ぎ機能：アカウント(メールアドレスとパスワード)を登録して、データの引継ぎが行えるようになりました。</p>
				</div>-->
				<!--
				________________________________
						Posts Content
				-->
				<?php if ( have_posts() ) : ?>
				<?php query_posts('showposts=10'); ?>
					<?php while ( have_posts() ) : the_post(); ?>
					<article class="<?php post_class(); ?>" id="post-<?php the_ID(); ?>">
						<div class="c-txt_accordion">
							<h3>
								<span><?php echo get_the_date(); ?></span>
								<?php echo get_the_title(); ?>
							</h3>
							<p><?php the_content(); ?></p>
						</div>
					</article>
					<?php endwhile; ?>
				<!--
				________________________________
				-->
			</div><!-- ▲ Text accordion ▲ -->
			<!--
			________________________________
			-->
			<div class="p-topics_1__btns">

				<?php next_posts_link( '<div class="page-link c-btn_link_1 c-btn_link_1__before u-fl">前へ</div>' ); ?>

				<?php previous_posts_link( '<div class="page-link c-btn_link_1 c-btn_link_1__after u-fr">次へ</div>' ); ?>

			</div>
			<?php endif; ?>
			<!--
			________________________________
			-->
		</div><!-- ▲ Container ▲ -->
	</section>
<!--
________________________________________________________________
-->
</main>

<?php get_footer('page'); ?>