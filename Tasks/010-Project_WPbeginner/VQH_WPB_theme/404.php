<?php get_header(); ?>

<div class="container"><!-- ▼ Container ▼ -->
	<div class="row"><!-- ▼ row ▼ -->

		<div class="col-md-8"><!-- ▼ Blog Entries Column ▼ -->
			<!--
			________________________________
					Title
			-->
			<h1 class="my-4">Hidden Page: 404<br>
				<small>You have found me ! The absolutely not found page.</small>
			</h1>
			<!--
			________________________________
					Content
			-->
			<br><br>
			<p>It looks like you have found absolutely nothing at this location. Maybe try a search ?</p>
			<?php get_search_form(); ?>
			<!--
			________________________________
			-->
		</div><!-- ▲ Blog Entries Column ▲ -->

		<?php get_sidebar(); ?>

	</div><!-- ▲ row ▲ -->
</div><!-- ▲ Container ▲ -->

<?php get_footer(); ?>

