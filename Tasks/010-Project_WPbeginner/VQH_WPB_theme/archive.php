<?php get_header(); ?>

<div class="container"><!-- ▼ Container ▼ -->
	<!-- o -->
	<div class="row"><!-- ▼ row ▼ -->
		<div class="col-md-8"><!-- ▼ Blog Entries Column ▼ -->
			<!-- o -->
			<h1 class="my-4"><small><u>Category:</u></small> <?php $page_object = get_queried_object(); ?><?php echo $page_object->cat_name; ?>
			</h1>
			<!--
			________________________________
			-->
			<?php if ( have_posts() ) : ?>
				<?php while ( have_posts() ) : the_post(); ?>
					<article class="<?php post_class(); ?>" id="post-<?php the_ID(); ?>">
						<div class="card mb-4">
							<!--
							________________________________
							-->
							<?php echo get_the_post_thumbnail( $page->ID, 'large','style=max-width:100%;height:auto;margin:auto' ); ?>
							<!--
							________________________________
							-->
							<div class="card-body">
								<h2 class="card-title"><?php the_title(); ?></h2>
								<p class="card-text"><?php the_content(); ?></p>
							</div>
							<!--
							________________________________
							-->
							<div class="card-footer text-muted">
								Posted on <?php the_date();?> by
								<a href="<?php get_author_posts_url( $author_id, $author_nicename ); ?>"><?php the_author();?></a>
							</div>
							<!--
							________________________________
							-->
						</div>
					</article>
				<?php endwhile; ?>

				<!-- Pagination -->
				<ul class="pagination justify-content-center mb-4">
					<li class="page-item">
						<?php next_posts_link( '<div class="page-link" href="#">&larr; Older</div>' ); ?>
					</li>

					<li class="page-item">
						<?php previous_posts_link( '<div class="page-link" href="#">Newer &rarr;</div>' ); ?>
					</li>
				</ul>

			<?php endif; ?>
			<!-- x -->
		</div><!-- ▲ Blog Entries Column ▲ -->
		<!--
		________________________________
		-->
		<?php get_sidebar(); ?>
		<!--
		________________________________
		-->
	</div><!-- ▲ row ▲ -->
	<!-- x -->
</div><!-- ▲ Container ▲ -->

<?php get_footer(); ?>
