<?php
$comments_arg = array(
	'form'	=> array(
		'class' => 'form-horizontal'
		),
	'fields' => apply_filters( 'comment_form_default_fields',
		array(
			'author'=> '<div class="form-group">'.
							'<input id="author" name="author" class="form-control" type="text" placeholder="(required) Username">
						</div>',

			'email' => '<div class="form-group">'.
							'<input type="email" id="email" name="email" class="form-control" type="text" placeholder="(required) E-mail">
						</div><br>' )
		),

		'comment_field'	=>	'<div class="form-group">' .
							'<textarea id="comment" class="form-control" name="comment" rows="3" aria-required="true" placeholder="(required) Please leave your fabulous thought in this awesome box !"></textarea>' .
							'</div>',

		'class_form'			=> 'card-body',
		'title_reply_before'	=> '<h5 class="card-header">',
		'comment_notes_after'	=> '</h5>',
		'title_reply'			=> 'Leave a Comment:',
		'title_reply_to'		=> 'Salty Reply',
		'cancel_reply_link'		=> 'Cancel',
		'class_submit'			=> 'btn btn-primary',
		'label_submit'			=> 'Submit',
		'comment_notes_before'	=> ''
);

comment_form($comments_arg);

?>

<?php if ( have_comments() ) : ?>

<div class="media mb-4">
	<div class="mb-4">
		<?php wp_list_comments(['callback' => 'WPbeginner_comments']) ?>
	</div>
</div>

<?php endif; ?>