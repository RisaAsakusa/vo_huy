<?php
/*
 * Template Name: Full, Comment
 * Template Post Type: post, page
 */

 get_header();	?>

		<!-- Page Content -->
		<div class="container">

			<div class="row">

				<!-- Post Content Column -->
				<div class="col-lg">
        <?php while ( have_posts() ) : the_post(); ?>
					<!-- Title -->
					<h1 class="mt-4"><?php the_title(); ?></h1>

					<!-- Author -->
					<p class="lead">
						by
						<a href="#"><?php the_author();?></a>
					</p>

					<hr>

					<!-- Date/Time -->
					<p>Posted on <?php the_date();?> at <?php the_time();?></p>

					<hr>

					<!-- Preview Image -->
					<?php echo get_the_post_thumbnail( $page->ID, 'large','style=max-width:100%;height:auto;margin:auto;display:block' ); ?>

					<hr>

					<!-- Post Content -->
					<p class="lead"><?php the_content(); ?></p>

					<hr>

  				<?php
  					if ( comments_open() || get_comments_number() ) :
  							comments_template();
  					endif;
  				?>
        <?php endwhile; ?>
				</div>

			</div>
			<!-- /.row -->

		</div>
		<!-- /.container -->
<?php get_footer(); ?>