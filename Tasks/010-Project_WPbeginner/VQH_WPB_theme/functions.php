<?php
//================================================================
//		[-] Styling with jQuery
//================================================================
?>
<script>
window.onload = function() {
	$('.comment-respond').addClass('card my-4');
	$('.nav-item a').addClass('nav-link');
	$("#wp-calendar").css({ "width": "100%", "text-align": "center"});
	$( ".card-body ul" ).addClass( "list-unstyled mb-0" );
	//$( ".card-body ul" ).css({ "display": "flex", "flex-wrap": "wrap"});
	$( ".card-body .menu-main-menu-container ul" ).css({ "display": "block", "flex-wrap": "none", "text-align": "center"});
	$(".cat-item").css({ "width": "50%", "float": "left"});
};
</script>

<?php

//================================================================
//		[0] Include
//================================================================
include('widget/wpb_test_custom_widget.php');





//================================================================
//		[1] Link: CSS + JS
//================================================================
function add_theme_css() {
	wp_enqueue_style( 'style', get_stylesheet_uri() );
	wp_enqueue_style( 'bootstrap.min', get_template_directory_uri() . '/vendor/bootstrap/css/bootstrap.min.css', array());
	wp_enqueue_style( 'blog-post', get_template_directory_uri() . '/css/blog-post.css', array());
}
add_action( 'wp_enqueue_scripts', 'add_theme_css' );

//function add_theme_js() {
//	wp_enqueue_script( 'jquery.min', get_template_directory_uri() . '/vendor/jquery/jquery.min.js', array ( 'jquery' ), true);
//	wp_enqueue_script( 'bootstrap.bundle.min', get_template_directory_uri() . '/vendor/bootstrap/js/bootstrap.bundle.min.js', array ( 'bootstrap' ), true);
//}
//add_action( 'wp_footer', 'add_theme_js' );





//================================================================
//		[2] Post Thumbnails
//================================================================
add_theme_support( 'post-thumbnails' );




//================================================================
//		[3] Homepage Post Excerpt
//
//		*Should disable this when enable "Function [4]"*
//================================================================
//function excerpt($limit) {
//	$excerpt = explode(' ', get_the_excerpt(), $limit);
//	if (count($excerpt)>=$limit) {
//		array_pop($excerpt);
//		$excerpt = implode(" ",$excerpt).'...';
//	} else {
//		$excerpt = implode(" ",$excerpt);
//	}
//	$excerpt = preg_replace('`[[^]]*]`','',$excerpt);
//	return $excerpt;
//}
//
//function content($limit) {
//	$content = explode(' ', get_the_content(), $limit);
//	if (count($content)>=$limit) {
//		array_pop($content);
//		$content = implode(" ",$content).'...';
//	} else {
//		$content = implode(" ",$content);
//	}
//	$content = preg_replace('/[.+]/','', $content);
//	$content = apply_filters('the_content', $content);
//	$content = str_replace(']]>', ']]&gt;', $content);
//	return $content;
//}




//================================================================
//		[4] Stying: "Read More" link
//
//		*Should disable this when enable "Function [3]"*
//
//		*Must insert "Read more tag (Shift + Alt + T)"
//			in every post to make this function work !*
//================================================================
function modify_read_more_link() {
    return '<br><br><a class="btn btn-primary" href="' . get_permalink() . '">Read More &rarr;</a>';
}
add_filter( 'the_content_more_link', 'modify_read_more_link' );




//================================================================
//		[5] Navigation Menu
//================================================================
function menu_gnavi() {
  register_nav_menu('header',__( 'Header' ));
}
add_action( 'init', 'menu_gnavi' );

function add_classes_on_li($classes, $item, $args) {
  $classes[] = 'nav-item';
  return $classes;
}
add_filter('nav_menu_css_class','add_classes_on_li',1,3);




//================================================================
//		[6] Left Side - Widget
//================================================================
function WPbeginner_widgets_init() {
	register_sidebar( array(
		'name'          => 'Sidebar',
		'id'            => 'sidebar-1',
		'description'   => '[WPbeginner] Add widgets here !',
		'before_title'	=> '<h5 class="card-header">',
		'after_title'   => '</h5><div class="card-body">',
		'before_widget' => '<div class="card my-4">',
		'after_widget'  => '</div></div>',
	) );
}
add_action( 'widgets_init', 'WPbeginner_widgets_init' );





//================================================================
//		[7] Comments
//================================================================
function WPbeginner_comments( $comment, $args, $depth ) {
	$GLOBALS['comment'] = $comment;
	?>
	<?php if ( $comment->comment_approved == '1' ): ?>
	<div class="media mb-4">
		<?php echo '<img class="d-flex mr-3 rounded-circle" src="'.get_avatar_url($comment).'" width="50" height="50">' ?>
		<div class="media-body">
			<?php echo  '<h5 class="mt-0"><a rel="nofllow" href="'.get_comment_author_url().'">'.get_comment_author().'</a></h5>' ?>
			<p class="mt-1">
				<?php comment_text() ?>
			</p>

			<div class="reply">
				<?php comment_reply_link(array_merge( $args, array('reply_text' => 'Reply','depth' => $depth, 'max_depth' => $args['max_depth']))) ?>
			</div>
		</div>
	</div>
	<?php endif;
}



?>