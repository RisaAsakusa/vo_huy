<!DOCTYPE html>
<html lang="en">

<head>

	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="description" content="">
	<meta name="author" content="">

	<title><?php bloginfo( ‘url’ ); ?><?php single_post_title(' » '); ?></title>

	<?php wp_head(); ?>



</head>

<body>

<!-- Navigation -->
<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
	<div class="container"><!-- ▼ Container ▼ -->
		<!--
		________________________________
				Logo
		-->
		<a class="navbar-brand" href="<?php echo get_home_url(); ?>"><?php bloginfo( ‘url’ ); ?></a>
		<!--
		________________________________
				Menu Button SP
		-->
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>
		<!--
		________________________________
				Navigation
		-->
		<div class="collapse navbar-collapse" id="navbarResponsive"><!-- ▼ navbar ▼ -->

			<!--<ul class="navbar-nav ml-auto">
				<li class="nav-item active">
					<a class="nav-link" href="#">Home
						<span class="sr-only">(current)</span>
					</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="#">About</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="#">Services</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="#">Contact</a>
				</li>
			</ul>-->

			<?php wp_nav_menu( array(
				'container' => 'false',
				'menu_class' => 'navbar-nav ml-auto',
			));
			?>

		</div><!-- ▲ navbar ▲ -->
		<!--
		________________________________
		-->
	</div><!-- ▲ Container ▲ -->
</nav>