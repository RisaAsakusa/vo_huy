<?php
/*
 * Template Name: Sidebar, Comment
 * Template Post Type: post
 */
get_header(); ?>

<div class="container"><!-- ▼ Container ▼ -->
	<div class="row"><!-- ▼ row ▼ -->

		<div class="col-lg-8"><!-- ▼ Post Content Column ▼ -->
			<?php while ( have_posts() ) : the_post(); ?>
				<!-- Title -->
				<h1 class="mt-4"><?php the_title(); ?></h1>

				<!-- Author -->
				<p class="lead">
					by
					<a href="#"><?php the_author();?></a>
				</p>
				<hr>

				<!-- Date/Time -->
				<p>Posted on <?php the_date();?> at <?php the_time();?></p>
				<hr>

				<!-- Preview Image -->
				<?php echo get_the_post_thumbnail( $page->ID, 'large','style=max-width:100%;height:auto;margin:auto;display:block' ); ?>
				<hr>

				<!-- Post Content -->
				<p class="lead"><?php the_content(); ?></p>
				<hr>

				<!-- Comments -->
				<?php
					if ( comments_open() || get_comments_number() ) :
							comments_template();
					endif;
				?>

				<!-- x -->
			<?php endwhile; ?>
		</div><!-- ▲ Post Content Column ▲ -->

		<?php get_sidebar(); ?>

	</div><!-- ▲ row ▲ -->
</div><!-- ▲ Container ▲ -->

<?php get_footer(); ?>