<?php get_header(); ?>

<div class="container"><!-- ▼ Container ▼ -->

	<div class="row"><!-- ▼ row ▼ -->
		<div class="col-lg-8"><!-- ▼ Post Content Column ▼ -->
			<!--
			________________________________
			-->
			<header class="page-header">
				<br><br><hr>
				<?php if ( have_posts() ) : ?>
					<!-- Title -->
					<h1 class="mt-4"><?php printf( __( 'Congratulation!<br><small>You have found: <span class="text-primary">%s</span>'), '<span>' . get_search_query() . '</span></small>' ); ?></h1>
				<?php else : ?>
					<!-- Title -->
					<h1 class="mt-4"><?php _e( 'Congratulation!<br><small>You have absolutely found Nothing !</small>'); ?></h1>
				<?php endif; ?>
				<br><hr><br><br>
			</header>
			<!--
			________________________________
			-->
			<?php while ( have_posts() ) : the_post(); ?>
				<!-- Title -->
				<h1 class="mt-4"><?php the_title(); ?></h1>

				<!-- Author -->
				<p class="lead">
					by
					<a href="#"><?php the_author();?></a>
				</p>
				<hr>

				<!-- Date/Time -->
				<p>Posted on <?php the_date();?> at <?php the_time();?></p>
				<hr>

				<!-- Preview Image -->
				<?php echo get_the_post_thumbnail( $page->ID, 'large','style=max-width:100%;height:auto;margin:auto;display:block' ); ?>
				<hr>

				<!-- Post Content -->
				<p class="lead"><?php the_content(); ?></p>
				<hr>

				<!-- x -->
			<?php endwhile; ?>
			<!--
			________________________________
			-->
		</div><!-- ▲ Post Content Column ▲ -->

		<?php get_sidebar(); ?>

	</div><!-- ▲ row ▲ -->
</div><!-- ▲ Container ▲ -->

<?php get_footer(); ?>