<?php $unique_id = esc_attr( uniqid( 'search-form-' ) ); ?>

	<form class="input-group" role="search" method="get" class="search-form" action="<?php echo esc_url( home_url( '/' ) ); ?>">
		<!--
		________________________________
		-->
		<input type="search" id="<?php echo $unique_id; ?>" class="form-control search-field" placeholder="Search for..." value="<?php echo get_search_query(); ?>" name="s">
		<!--
		________________________________
		-->
		<span class="input-group-btn">
			<button class="btn btn-secondary search-submit" type="submit">Go!</button>
		</span>
		<!--
		________________________________
		-->
	</form>