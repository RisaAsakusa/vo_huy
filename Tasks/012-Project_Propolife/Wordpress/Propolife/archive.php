<?php get_header('page'); ?>
<body>
<!--
================================================================








================================================================
  __  __              _____   _   _
 |  \/  |     /\     |_   _| | \ | |
 | \  / |    /  \      | |   |  \| |
 | |\/| |   / /\ \     | |   | . ` |
 | |  | |  / ____ \   _| |_  | |\  |
 |_|  |_| /_/    \_\ |_____| |_| \_|

	@main
================================================================
-->
<main class="l-main p-page">
<!--
================================================================
	S E C T I O N    B A N N E R

	@secbanner    @banner
================================================================
-->
<section class="p-page_banner" style="background: url('<?php echo get_template_directory_uri(); ?>/assets/img/archive.jpg')">
	<h2 style="display: none;">Archive</h2>
</section>
<!--
================================================================








================================================================
	S E C T I O N    Content

	@secc
================================================================
-->
<!--
________________________________________________________________
-->
<section class="p-page_content"><!-- ▼ Content ▼ -->
<div class="l-container__narrow"><!-- ▼ Container ▼ -->

<h2 class="p-page_content__title">カテゴリー：
	<?php $page_object = get_queried_object(); ?>
	<?php echo $page_object->cat_name; ?>
</h2>
<!--
________________________________________________________________
		Style
-->
<style>
	.c-txtimg_topics ul li:nth-child(even) a {flex-direction: row;}
</style>
<!--
________________________________________________________________
		Content: Found the Result
-->
<div class="p-page_content__content"><!-- ▼ Content ▼ -->
	<div class="c-txtimg_topics"><!-- ▼ Topics List ▼ -->
		<ul>
			<!--
			________________________________
					Cards
			-->
<?php if ( have_posts() ) : ?>
				<?php while ( have_posts() ) : the_post(); ?>
					<li class="<?php post_class(); ?>" id="post-<?php the_ID(); ?>">
						<a href="<?php echo get_permalink( $post->ID ); ?>">

							<!-- Image -->
							<?php $backgroundImg = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' ); ?>
							<div class="c-txtimg_topics__img" style="background: url('<?php echo $backgroundImg[0] ?>')"></div>

							<!-- Text -->
							<div class="c-txtimg_topics__content">
								<span>
									<h3><?php the_title(); ?></h3>
									<p><?php the_date();?></p>
									<p><?php the_excerpt(); ?></p>
								</span>
							</div>

							<!-- x -->
						</a>
					</li>
				<?php endwhile; ?>
			<!--
			________________________________
			-->
		</ul>
	</div><!-- ▲ Topics List ▲ -->
</div><!-- ▲ Content ▲ -->
<!--
________________________________________________________________
		Pagination
-->
<ul class="c-mics_pagination">
	<li>
		<?php next_posts_link( '<div class="page-link" href="#">&larr; Older</div>' ); ?>
	</li>

	<li>
		<?php previous_posts_link( '<div class="page-link" href="#">Newer &rarr;</div>' ); ?>
	</li>
</ul>
<?php endif; ?>
<!--
________________________________________________________________
-->
</div><!-- ▲ Container ▲ -->
</section><!-- ▲ Content ▲ -->



<!--
________________________________________________________________
		Scroll to Top Button
-->
<div class="l-btn_top">
	<div class="c-btn_top"><a></a></div>
</div>
<!--
________________________________________________________________
-->
</main>
</body>
<?php get_footer('page'); ?>