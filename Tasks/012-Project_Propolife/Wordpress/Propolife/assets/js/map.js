google.maps.event.addDomListener(window, 'load', init);

function init() {

// Basic options for a simple Google Map
// For more options see: https://developers.google.com/maps/documentation/javascript/reference#MapOptions
var mapOptions = {
	// How zoomed in you want the map to start at (always required)
	zoom: 18,

	// The latitude and longitude to center the map (always required)
	center: new google.maps.LatLng(35.681476, 139.762688),

//================================================================
//		Styling the Map
//================================================================
//________________________________________________________
	styles:
			//--▼-- Custom Style --▼--
			[
				{
					"featureType": "administrative",
					"elementType": "all",
					"stylers": [
						{
							"saturation": "-100"
						}
					]
				},
				{
					"featureType": "administrative.province",
					"elementType": "all",
					"stylers": [
						{
							"visibility": "off"
						}
					]
				},
				{
					"featureType": "landscape",
					"elementType": "all",
					"stylers": [
						{
							"saturation": -100
						},
						{
							"lightness": 65
						},
						{
							"visibility": "on"
						}
					]
				},
				{
					"featureType": "poi",
					"elementType": "all",
					"stylers": [
						{
							"saturation": -100
						},
						{
							"lightness": "50"
						},
						{
							"visibility": "simplified"
						}
					]
				},
				{
					"featureType": "road",
					"elementType": "all",
					"stylers": [
						{
							"saturation": "-100"
						}
					]
				},
				{
					"featureType": "road.highway",
					"elementType": "all",
					"stylers": [
						{
							"visibility": "simplified"
						}
					]
				},
				{
					"featureType": "road.arterial",
					"elementType": "all",
					"stylers": [
						{
							"lightness": "30"
						}
					]
				},
				{
					"featureType": "road.local",
					"elementType": "all",
					"stylers": [
						{
							"lightness": "40"
						}
					]
				},
				{
					"featureType": "transit",
					"elementType": "all",
					"stylers": [
						{
							"saturation": -100
						},
						{
							"visibility": "simplified"
						}
					]
				},
				{
					"featureType": "water",
					"elementType": "geometry",
					"stylers": [
						{
							"hue": "#ffff00"
						},
						{
							"lightness": -25
						},
						{
							"saturation": -97
						}
					]
				},
				{
					"featureType": "water",
					"elementType": "labels",
					"stylers": [
						{
							"lightness": -25
						},
						{
							"saturation": -100
						}
					]
				}
			]
			//--▲-- Custom Style --▲--
		};

//________________________________________________________




//================================================================
//		Get the HTML DOM element that will contain your map
//
//		* Must be use "id" to make this work !
//================================================================
//________________________________________________________
//---- 💻 PC ----
var mapElement = document.getElementById('google_map');
//---- 📱 SP ----
var mapElement_sp = document.getElementById('google_map_sp');

//________________________________________________________




//================================================================
//		Create the Google Map
//================================================================
//________________________________________________________
//---- 💻 PC ----
var map = new google.maps.Map(mapElement, mapOptions);
//---- 📱 SP ----
var map_sp = new google.maps.Map(mapElement_sp, mapOptions);

//________________________________________________________




//================================================================
//		Marker Icon
//================================================================
//________________________________________________________
//---- 💻 PC ----
var markerIcon = {
	url: '<?php echo get_template_directory_uri(); ?>/assets/img/pc/marker.png',
	scaledSize: new google.maps.Size(457, 113),
};
//---- 📱 SP ----
var markerIcon_sp = {
	url: '<?php echo get_template_directory_uri(); ?>/assets/img/pc/marker.png',
	scaledSize: new google.maps.Size(228.5, 56.5),
};

//________________________________________________________




//================================================================
//		Put Marker Icon into Map
//================================================================
//________________________________________________________
//---- 💻 PC ----
var marker = new google.maps.Marker({
	position: new google.maps.LatLng(35.681476, 139.762688),
	map: map,
	icon: markerIcon,
	title: 'Propolife',
});
//---- 📱 SP ----
var marker_sp = new google.maps.Marker({
	position: new google.maps.LatLng(35.681476, 139.762688),
	map: map_sp,
	icon: markerIcon_sp,
	title: 'Propolife',
});

//________________________________________________________
}
// THE END of the Function !

