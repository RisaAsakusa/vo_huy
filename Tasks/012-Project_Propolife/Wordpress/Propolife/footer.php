<!--
================================================================








================================================================
  ______    ____     ____    _______   ______   _____
 |  ____|  / __ \   / __ \  |__   __| |  ____| |  __ \
 | |__    | |  | | | |  | |    | |    | |__    | |__) |
 |  __|   | |  | | | |  | |    | |    |  __|   |  _  /
 | |      | |__| | | |__| |    | |    | |____  | | \ \
 |_|       \____/   \____/     |_|    |______| |_|  \_\

	@footer
================================================================
-->
<footer class="l-footer">
<!--
________________________________________________________________
		💻 PC Navigation
-->
<div class="pc_only">
	<div class="c-gnavi c-gnavi--bottom_line"><!-- ▼ Navigation ▼ -->
		<nav>
			<ul>
				<li><a href="#">ABOUT</a></li>
				<li><a href="#">CONCEPT</a></li>
				<li><a href="#">TOPICS</a></li>
				<li><a href="#">FAQ</a></li>
				<li><a href="#">ACCESS</a></li>
			</ul>
		</nav>
	</div><!-- ▲ Navigation ▲ -->
</div>

<!--
________________________________________________________________
		Contact
-->
<div class="c-footer_contact"><!-- ▼ c-footer_contact ▼ -->
	<!--
	________________________________
			Logo
	-->
	<a class="u-hover_eff_1 pc_only" href="<?php echo site_url(); ?>">
		<img src="<?php echo get_template_directory_uri(); ?>/assets/img/pc/logo.png" width="250" height="46" alt>
	</a>
	<!--
	________________________________
			Contacts
	-->
	<div class="c-footer_contact__content"><!-- ▼ Content ▼ -->

		<!--
		________________________________
				Phone
		-->
		<div class="c-txt_phone">
			<h3>03-5555-6666</h3>
			<p>受付：10:00~18:00</p>
		</div>

		<!--
		________________________________
				Button
		-->
		<a class="c-btn_reservation" href="#">RESERVATION ご予約</a>

		<!--
		________________________________
				📱 SP Social Buttons
		-->
		<div class="l-btn_social l-btn_social__footer sp_only"><!-- ▼ Social Buttons ▼ -->

			<!-- Facebook -->
			<a class="c-btn_social" href="#" target="_blank">
				<img src="<?php echo get_template_directory_uri(); ?>/assets/img/sp/facebook.png" width="92" height="92" alt>
			</a>

			<!-- Instagram -->
			<a class="c-btn_social" href="#" target="_blank">
				<img src="<?php echo get_template_directory_uri(); ?>/assets/img/sp/instagram.png" width="92" height="92" alt>
			</a>

			<!-- Line -->
			<a class="c-btn_social" href="#" target="_blank">
				<img src="<?php echo get_template_directory_uri(); ?>/assets/img/sp/line.png" width="92" height="92" alt>
			</a>

			<!-- x -->
		</div><!-- ▲ Social Buttons ▲ -->
		<!--
		________________________________
		-->
	</div><!-- ▲ Content ▲ -->
	<!--
	________________________________
	-->
</div><!-- ▲ c-footer_contact ▲ -->
<!--
________________________________________________________________
		Copyright
-->
<div class="c-footer_copyright">
	<p>Copyright © PROPOLIFE HOTEL’S All Rights Reserved.</p>
</div>
<!--
________________________________________________________________
-->
</footer>


</body>
</html>