<!DOCTYPE html>
<html lang="ja">
<head>
<!-- Meta -->
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="旅先の“我が家”を創る">
<meta name="keywords" content="Propolife">
<!-- Site Title -->
<title>Propolife</title>
<!-- Favicon -->
<link rel="icon" href="<?php echo get_template_directory_uri(); ?>/assets/img/favicon.png" type="image/png">
<!-- CSS -->
<link href="<?php echo get_template_directory_uri(); ?>/assets/css/style.css" rel="stylesheet">
<!-- JS -->
<script src="<?php echo get_template_directory_uri(); ?>/assets/js/common.js"></script>
<!--
================================================================
-->
</head>
<body>
<!--
================================================================








================================================================
  _    _   ______              _____    ______   _____
 | |  | | |  ____|     /\     |  __ \  |  ____| |  __ \
 | |__| | | |__       /  \    | |  | | | |__    | |__) |
 |  __  | |  __|     / /\ \   | |  | | |  __|   |  _  /
 | |  | | | |____   / ____ \  | |__| | | |____  | | \ \
 |_|  |_| |______| /_/    \_\ |_____/  |______| |_|  \_\

	@header
================================================================
-->
<header class="l-header">
<!--
________________________________________________________________
-->
<div class="c-header"><!-- ▼ c-header ▼ -->
	<!--
	________________________________
			Logo
	-->
	<a class="c-header__logo u-hover_eff_1" href="<?php echo site_url(); ?>">
		<!-- 💻 PC -->
		<img class="pc_only" src="<?php echo get_template_directory_uri(); ?>/assets/img/pc/logo.png" width="250" height="46" alt>
		<!-- 📱 SP -->
		<img class="sp_only" src="<?php echo get_template_directory_uri(); ?>/assets/img/sp/logo.png" width="586" height="106" alt>
	</a>
	<!--
	________________________________
			Social Buttons
	-->
	<div class="l-btn_social pc_only"><!-- ▼ Social Buttons ▼ -->

		<!-- Facebook -->
		<a class="c-btn_social" href="#" target="_blank">
			<img src="<?php echo get_template_directory_uri(); ?>/assets/img/pc/facebook.png" width="18" height="18" alt>
		</a>

		<!-- Instagram -->
		<a class="c-btn_social" href="#" target="_blank">
			<img src="<?php echo get_template_directory_uri(); ?>/assets/img/pc/instagram.png" width="18" height="18" alt>
		</a>

		<!-- Line -->
		<a class="c-btn_social" href="#" target="_blank">
			<img src="<?php echo get_template_directory_uri(); ?>/assets/img/pc/line.png" width="18" height="18" alt>
		</a>

		<!-- x -->
	</div><!-- ▲ Social Buttons ▲ -->
	<!--
	________________________________
	-->
</div><!-- ▲ c-header ▲ -->
<!--
________________________________________________________________
		📱 SP Menu Button
-->
<div class="c-btn_MenuSP sp_only" onclick="open_SP_menu(this)">
	<div class="c-btn_MenuSP__bar1"></div>
	<div class="c-btn_MenuSP__bar2"></div>
	<div class="c-btn_MenuSP__bar3"></div>
</div>
<!--
________________________________________________________________
		Global Navigation
-->
<div class="c-gnavi c-gnavi__with_btn c-gnavi--top_line"><!-- ▼ Global Navigation ▼ -->
	<nav>
		<!-- Navigation -->
		<ul>
			<li><a href="<?php echo site_url(); ?>#about">ABOUT</a></li>
			<li><a href="<?php echo site_url(); ?>#concept">CONCEPT</a></li>
			<li><a href="<?php echo site_url(); ?>#topics">TOPICS</a></li>
			<li><a href="<?php echo site_url(); ?>#faq">FAQ</a></li>
			<li><a href="<?php echo site_url(); ?>#access">ACCESS</a></li>
		</ul>

		<!-- Button -->
		<div class="pc_only">
			<a class="c-btn_reservation" href="#">RESERVATION ご予約</a>
		</div>

		<!-- x -->
	</nav>
</div><!-- ▲ Global Navigation ▲ -->
<!--
________________________________________________________________
-->
</header>