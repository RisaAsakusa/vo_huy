<!DOCTYPE html>
<html lang="ja">
<head>
<!-- Meta -->
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="旅先の“我が家”を創る">
<meta name="keywords" content="Propolife">
<!-- Site Title -->
<title>Propolife</title>
<!-- Favicon -->
<link rel="icon" href="<?php echo get_template_directory_uri(); ?>/assets/img/favicon.png" type="image/png">
<!-- CSS -->
<link href="<?php echo get_template_directory_uri(); ?>/assets/css/style.css" rel="stylesheet">
<!-- JS -->
<script src="<?php echo get_template_directory_uri(); ?>/assets/js/common.js"></script>
<!--
================================================================

You need to include this script tag on any page that has a Google Map.

The following script tag will work when opening this example locally on your computer.

But if you use this on a localhost server or a live website you will need to include an API key.

Sign up for one here (it's free for small usage):

    https://developers.google.com/maps/documentation/javascript/tutorial#api_key

After you sign up, use the following script tag with YOUR_GOOGLE_API_KEY replaced with your actual key.

	<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=YOUR_GOOGLE_API_KEY&sensor=false"></script>

-->

<script src="https://maps.googleapis.com/maps/api/js?sensor=false"></script>
<!--
* This script must be under "googleapis" script to make the function work !
-->
<script>
google.maps.event.addDomListener(window, 'load', init);

function init() {

// Basic options for a simple Google Map
// For more options see: https://developers.google.com/maps/documentation/javascript/reference#MapOptions
var mapOptions = {
	// How zoomed in you want the map to start at (always required)
	zoom: 18,

	// The latitude and longitude to center the map (always required)
	center: new google.maps.LatLng(35.681476, 139.762688),

//================================================================
//		Styling the Map
//================================================================
//________________________________________________________
	styles:
			//--▼-- Custom Style --▼--
			[
				{
					"featureType": "administrative",
					"elementType": "all",
					"stylers": [
						{
							"saturation": "-100"
						}
					]
				},
				{
					"featureType": "administrative.province",
					"elementType": "all",
					"stylers": [
						{
							"visibility": "off"
						}
					]
				},
				{
					"featureType": "landscape",
					"elementType": "all",
					"stylers": [
						{
							"saturation": -100
						},
						{
							"lightness": 65
						},
						{
							"visibility": "on"
						}
					]
				},
				{
					"featureType": "poi",
					"elementType": "all",
					"stylers": [
						{
							"saturation": -100
						},
						{
							"lightness": "50"
						},
						{
							"visibility": "simplified"
						}
					]
				},
				{
					"featureType": "road",
					"elementType": "all",
					"stylers": [
						{
							"saturation": "-100"
						}
					]
				},
				{
					"featureType": "road.highway",
					"elementType": "all",
					"stylers": [
						{
							"visibility": "simplified"
						}
					]
				},
				{
					"featureType": "road.arterial",
					"elementType": "all",
					"stylers": [
						{
							"lightness": "30"
						}
					]
				},
				{
					"featureType": "road.local",
					"elementType": "all",
					"stylers": [
						{
							"lightness": "40"
						}
					]
				},
				{
					"featureType": "transit",
					"elementType": "all",
					"stylers": [
						{
							"saturation": -100
						},
						{
							"visibility": "simplified"
						}
					]
				},
				{
					"featureType": "water",
					"elementType": "geometry",
					"stylers": [
						{
							"hue": "#ffff00"
						},
						{
							"lightness": -25
						},
						{
							"saturation": -97
						}
					]
				},
				{
					"featureType": "water",
					"elementType": "labels",
					"stylers": [
						{
							"lightness": -25
						},
						{
							"saturation": -100
						}
					]
				}
			]
			//--▲-- Custom Style --▲--
		};

//________________________________________________________




//================================================================
//		Get the HTML DOM element that will contain your map
//
//		* Must be use "id" to make this work !
//================================================================
//________________________________________________________
//---- 💻 PC ----
var mapElement = document.getElementById('google_map');
//---- 📱 SP ----
var mapElement_sp = document.getElementById('google_map_sp');

//________________________________________________________




//================================================================
//		Create the Google Map
//================================================================
//________________________________________________________
//---- 💻 PC ----
var map = new google.maps.Map(mapElement, mapOptions);
//---- 📱 SP ----
var map_sp = new google.maps.Map(mapElement_sp, mapOptions);

//________________________________________________________




//================================================================
//		Marker Icon
//================================================================
//________________________________________________________
//---- 💻 PC ----
var markerIcon = {
	url: '<?php echo get_template_directory_uri(); ?>/assets/img/pc/marker.png',
	scaledSize: new google.maps.Size(457, 113),
};
//---- 📱 SP ----
var markerIcon_sp = {
	url: '<?php echo get_template_directory_uri(); ?>/assets/img/pc/marker.png',
	scaledSize: new google.maps.Size(228.5, 56.5),
};

//________________________________________________________




//================================================================
//		Put Marker Icon into Map
//================================================================
//________________________________________________________
//---- 💻 PC ----
var marker = new google.maps.Marker({
	position: new google.maps.LatLng(35.681476, 139.762688),
	map: map,
	icon: markerIcon,
	title: 'Propolife',
});
//---- 📱 SP ----
var marker_sp = new google.maps.Marker({
	position: new google.maps.LatLng(35.681476, 139.762688),
	map: map_sp,
	icon: markerIcon_sp,
	title: 'Propolife',
});

//________________________________________________________
}
// THE END of the Function !
</script>
<!--
================================================================
-->

</head>
<body>
<!--
================================================================








================================================================
  _    _   ______              _____    ______   _____
 | |  | | |  ____|     /\     |  __ \  |  ____| |  __ \
 | |__| | | |__       /  \    | |  | | | |__    | |__) |
 |  __  | |  __|     / /\ \   | |  | | |  __|   |  _  /
 | |  | | | |____   / ____ \  | |__| | | |____  | | \ \
 |_|  |_| |______| /_/    \_\ |_____/  |______| |_|  \_\

	@header
================================================================
-->
<header class="l-header">
<!--
________________________________________________________________
-->
<div class="c-header"><!-- ▼ c-header ▼ -->
	<!--
	________________________________
			Logo
	-->
	<a class="c-header__logo u-hover_eff_1" href="<?php echo site_url(); ?>">
		<!-- 💻 PC -->
		<img class="pc_only" src="<?php echo get_template_directory_uri(); ?>/assets/img/pc/logo.png" width="250" height="46" alt>
		<!-- 📱 SP -->
		<img class="sp_only" src="<?php echo get_template_directory_uri(); ?>/assets/img/sp/logo.png" width="586" height="106" alt>
	</a>
	<!--
	________________________________
			Social Buttons
	-->
	<div class="l-btn_social pc_only"><!-- ▼ Social Buttons ▼ -->

		<!-- Facebook -->
		<a class="c-btn_social" href="#" target="_blank">
			<img src="<?php echo get_template_directory_uri(); ?>/assets/img/pc/facebook.png" width="18" height="18" alt>
		</a>

		<!-- Instagram -->
		<a class="c-btn_social" href="#" target="_blank">
			<img src="<?php echo get_template_directory_uri(); ?>/assets/img/pc/instagram.png" width="18" height="18" alt>
		</a>

		<!-- Line -->
		<a class="c-btn_social" href="#" target="_blank">
			<img src="<?php echo get_template_directory_uri(); ?>/assets/img/pc/line.png" width="18" height="18" alt>
		</a>

		<!-- x -->
	</div><!-- ▲ Social Buttons ▲ -->
	<!--
	________________________________
	-->
</div><!-- ▲ c-header ▲ -->
<!--
________________________________________________________________
		📱 SP Menu Button
-->
<div class="c-btn_MenuSP sp_only" onclick="open_SP_menu(this)">
	<div class="c-btn_MenuSP__bar1"></div>
	<div class="c-btn_MenuSP__bar2"></div>
	<div class="c-btn_MenuSP__bar3"></div>
</div>
<!--
________________________________________________________________
		Global Navigation
-->
<div class="c-gnavi c-gnavi__with_btn c-gnavi--top_line"><!-- ▼ Global Navigation ▼ -->
	<nav>
		<!-- Navigation -->
		<ul>
			<li><a href="#">ABOUT</a></li>
			<li><a href="#">CONCEPT</a></li>
			<li><a href="#">TOPICS</a></li>
			<li><a href="#">FAQ</a></li>
			<li><a href="#">ACCESS</a></li>
		</ul>

		<!-- Button -->
		<div class="pc_only">
			<a class="c-btn_reservation" href="#">RESERVATION ご予約</a>
		</div>

		<!-- x -->
	</nav>
</div><!-- ▲ Global Navigation ▲ -->
<!--
________________________________________________________________
-->
</header>