<?php get_header('page'); ?>

<!--
================================================================








================================================================
  __  __              _____   _   _
 |  \/  |     /\     |_   _| | \ | |
 | \  / |    /  \      | |   |  \| |
 | |\/| |   / /\ \     | |   | . ` |
 | |  | |  / ____ \   _| |_  | |\  |
 |_|  |_| /_/    \_\ |_____| |_| \_|

	@main
================================================================
-->
<main class="l-main p-page">
<!--
================================================================
	S E C T I O N    B A N N E R

	@secbanner    @banner
================================================================
-->
<section class="p-page_banner" style="background: url('<?php echo get_template_directory_uri(); ?>/assets/img/search.jpg')">
	<h2 style="display: none;">Search</h2>
</section>
<!--
================================================================








================================================================
	S E C T I O N    Content

	@secc
================================================================
-->
<!--
________________________________________________________________
-->
<section class="p-page_content"><!-- ▼ Content ▼ -->
<div class="l-container__narrow"><!-- ▼ Container ▼ -->
<!--
________________________________________________________________
		Title
-->
<?php if ( have_posts() ) : ?>

	<h2 class="p-page_content__title">検索結果："<span class="u-txtdeco_underline"><?php echo get_search_query(); ?></span>"</h2>

	<?php else : ?>

<!--
________________________________________________________________
		Content: Impossible to find wanted result !
-->
<h2 class="p-page_content__title">申し訳ありません、私たちのデータベースに "<span class="u-txtdeco_underline"><?php echo get_search_query(); ?></span>" が見つかりません！</h2>

<div class="p-page_content__content" style="text-align: center;">
	サイトに戻る：<a href="<?php echo site_url(); ?>">Propolife</a>
</div>

<?php get_search_form(); ?>
<!--
________________________________________________________________
-->
<?php endif; ?>
<!--
________________________________________________________________
		Style
-->
<style>
	.c-txtimg_topics ul li:nth-child(even) a {flex-direction: row;}
</style>
<!--
________________________________________________________________
		Content: Found the Result
-->
<div class="p-page_content__content"><!-- ▼ Content ▼ -->
	<div class="c-txtimg_topics"><!-- ▼ Topics List ▼ -->
		<ul>
			<!--
			________________________________
					Cards
			-->
			<?php if ( have_posts() ) : ?>
			<?php query_posts('showposts=10'); ?>
				<?php while ( have_posts() ) : the_post(); ?>
					<li class="<?php post_class(); ?>" id="post-<?php the_ID(); ?>">
						<a href="<?php echo get_permalink( $post->ID ); ?>">

							<!-- Image -->
							<?php $backgroundImg = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' ); ?>
							<div class="c-txtimg_topics__img" style="background: url('<?php echo $backgroundImg[0] ?>')"></div>

							<!-- Text -->
							<div class="c-txtimg_topics__content">
								<span>
									<h3><?php the_title(); ?></h3>
									<p><?php the_date();?></p>
									<p><?php the_excerpt(); ?></p>
								</span>
							</div>

							<!-- x -->
						</a>
					</li>
				<?php endwhile; ?>
			<?php endif; ?>
			<!--
			________________________________
			-->
		</ul>
	</div><!-- ▲ Topics List ▲ -->
</div><!-- ▲ Content ▲ -->
<!--
________________________________________________________________
		Pagination
-->
<ul class="c-mics_pagination">
	<li>
		<?php next_posts_link( '<div class="page-link" href="#">&larr; Older</div>' ); ?>
	</li>

	<li>
		<?php previous_posts_link( '<div class="page-link" href="#">Newer &rarr;</div>' ); ?>
	</li>
</ul>
<!--
________________________________________________________________
-->
</div><!-- ▲ Container ▲ -->
</section><!-- ▲ Content ▲ -->

<!--
________________________________________________________________
		Scroll to Top Button
-->
<div class="l-btn_top">
	<div class="c-btn_top"><a></a></div>
</div>
<!--
________________________________________________________________
-->
</main>

<?php get_footer('page'); ?>