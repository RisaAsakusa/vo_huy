<?php $unique_id = esc_attr( uniqid( 'search-form-' ) ); ?>

<form class="c-mics_search" role="search" method="get" action="<?php echo esc_url( home_url( '/' ) ); ?>">
	<!--
	________________________________
			Search Bar
	-->
	<input type="search" id="<?php echo $unique_id; ?>" placeholder="または検索を試みる...
" value="<?php echo get_search_query(); ?>" name="s">
	<!--
	________________________________
			Button
	-->
	<button type="submit">サーチ</button>
	<!--
	________________________________
	-->
</form>