<?php get_header('page'); ?>
<body>
<!--
================================================================








================================================================
  __  __              _____   _   _
 |  \/  |     /\     |_   _| | \ | |
 | \  / |    /  \      | |   |  \| |
 | |\/| |   / /\ \     | |   | . ` |
 | |  | |  / ____ \   _| |_  | |\  |
 |_|  |_| /_/    \_\ |_____| |_| \_|

	@main
================================================================
-->
<main class="l-main p-page">
<!--
================================================================
	S E C T I O N    B A N N E R

	@secbanner    @banner
================================================================
-->
<?php while ( have_posts() ) : the_post();?>

<?php $backgroundImg = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' ); ?>

<section class="p-page_banner" style="background: url('<?php echo $backgroundImg[0] ?>')">
	<h2 style="display: none;"><?php the_title(); ?></h2>
</section>
<!--
================================================================








================================================================
	S E C T I O N    Content

	@secc
================================================================
-->
<!--
________________________________________________________________
-->
<section class="p-page_content"><!-- ▼ Content ▼ -->
<div class="l-container__narrow"><!-- ▼ Container ▼ -->
<!--
________________________________________________________________
-->
	<h2 class="p-page_content__title"><?php the_title(); ?></h2>

	<h3 class="p-page_content__date">
		<span><?php the_date();?></span>
		<span>Category:&nbsp;<?php the_category( $parents ); ?></span>
	</h3>

	<div class="p-page_content__content"><?php the_content(); ?></div>
<!--
________________________________________________________________
-->
</div><!-- ▲ Container ▲ -->
</section><!-- ▲ Content ▲ -->

<?php endwhile; ?>

<!--
________________________________________________________________
		Scroll to Top Button
-->
<div class="l-btn_top">
	<div class="c-btn_top"><a></a></div>
</div>
<!--
________________________________________________________________
-->
</main>
</body>
<?php get_footer('page'); ?>