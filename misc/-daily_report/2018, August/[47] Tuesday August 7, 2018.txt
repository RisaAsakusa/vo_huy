Dear Ms. Risa Asakusa san,
This is my Daily report.

----------------
Date: Tuesday August 7, 2018 (7/8/2018)
Days: 47
Full Name: Võ Quốc Huy
----------------

+ Task010-Project_Logmansion
	- Completed: 100%
	
+ Task012-Project_Propolife
	- Completed: 5%
	
----------------

Detail:

	+ Task010-Project_Logmansion
		- Completed: "section 15", "section 16".
		
	+ Task012-Project_Propolife
		- Converted "psd" to "eps".
		- Export to img src.
		- Analyzed.
		- Completed 33%: "-header".


-----------------
Goodbye and See you tomorrow !